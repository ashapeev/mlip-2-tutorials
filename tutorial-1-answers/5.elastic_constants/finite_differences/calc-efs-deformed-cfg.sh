#!/bin/bash

export OMP_NUM_THREADS=1

cd MLIP

for ((i = 1; i <= 4; i++ ))
do
../../../mlp calc-efs pot.mtp "$i".cfg "$i"_efs.cfg
done

cd ../
