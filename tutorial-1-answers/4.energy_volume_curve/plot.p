set terminal png #portrait enhanced color dashed lw 1 "Helvetica" 24
set ticslevel 0
set output "E_V.png"
plot "E_V.txt" using 1:2 w li lt rgb 'red' lw 3 title "MTP", \
	 "E_V_vasp.txt" using 1:2 w li lt rgb 'blue' lw 4 title "VASP"

